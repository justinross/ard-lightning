#include <StandardCplusplus.h>
#include <vector>
#include <FastLED.h>

#include "config.h"

CRGB leds[NUM_LEDS];
unsigned long lastFrameAt;
int framerate = 100;
int frameLength = round(1000 / framerate);

class Burst
{
    int flashStart;
    int flashEnd;
    int lifeSpan;
    int pixelLife;
    int brightness;
    bool readyForDelete = false;
    bool done = false;
    unsigned long fadeStartedAt;
    unsigned long fadeDuration;

    public:
    Burst(int fStart, int fEnd, int life){
        Serial.println("New Burst()");

        if (fStart < 0) {
          fStart = 0;
        }
        if (fEnd >= NUM_LEDS) {
          fEnd = NUM_LEDS - 1;
        }
        
        flashStart = fStart;
        flashEnd = fEnd;
        lifeSpan = life;
        //how many pixels are lit?
        int width = flashEnd - flashStart + 1;
        fadeDuration = round((life / width) * 2);
        for(int x = 0;x < width; x++){
            leds[flashStart + x] = 0xFFFFFF;
        }
 
        fadeStartedAt = millis();
    }

    //destructor
    ~Burst(){
        Serial.println("destructed");
    }

    void Update(){
        if(!done){
            fadeCurrentPixels();
        }
    }

    void fadeCurrentPixels(){
        brightness = map( millis() - fadeStartedAt, 0, fadeDuration, 255, 0);
        // Serial.print("millis: ");
        // Serial.println(millis());
        // Serial.print("fadeStartedAt: ");
        // Serial.println(fadeStartedAt);
        // Serial.print("fadeDuration: ");
        // Serial.println(fadeDuration);
        // Serial.print("fading ");
        // Serial.print(flashStart);
        // Serial.print(" and ");
        // Serial.print(flashEnd);
        // Serial.print(" to ");
        // Serial.println(brightness);
        if(brightness <= 1){
            leds[flashStart] = 0x000000;
            leds[flashEnd] = 0x000000;
            setNextPixels();
        }
        else{
            leds[flashStart] = CRGB(brightness, brightness, brightness);
            leds[flashEnd] = CRGB(brightness, brightness, brightness);
        }
    }

    void setNextPixels(){
        if(flashStart > flashEnd){
            destroyBurst();
        }
        else if(flashStart == flashEnd){
            destroyBurst();
        }
        fadeStartedAt = millis();
        flashStart = flashStart + 1;
        flashEnd = flashEnd -1;
    }

    void destroyBurst(){
        done = true;
        Serial.print("DESTROYING\n");
        readyForDelete = true;
    }

    bool isReadyForDelete(){
        return readyForDelete;
    }

};

// const int maxAftershocks = 4;
// const int minAftershocks = 0;
// const float maxASDelay = .1;
// const float minASDelay = .01;

int buttonState = 0;
bool armed = true;
const int minWidth = 4;
const int maxWidth = 10;
const int minLife = 100;
const int maxLife = 500;
std::vector<Burst*> bursts;

void setup(){
    //safety delay
    delay(3000);
    FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
}

void loop(){
    buttonState = digitalRead(BUTTON_PIN);
    if (buttonState == HIGH) {
        if(armed == true){
            armed = false;
            //make a new burst
            int width = random(minWidth, maxWidth);
            int life = random(minLife, maxLife);
            int startLED = random(NUM_LEDS);
            int endLED = startLED + width;
            bursts.push_back(new Burst(startLED, endLED, life));

            //Force a redraw this frame.
            lastFrameAt = 0;
        }
    }
    else {
        armed = true;
    }
    for(int x = 0; x < bursts.size(); x++){
        bursts[x]->Update();
    }

    cleanupBursts();
    
    if(millis() - lastFrameAt > frameLength) {
        lastFrameAt = millis();
        FastLED.show();
    }
}

// Delete any bursts that were marked for delete
void cleanupBursts() {
    for(std::vector<Burst*>::iterator iter = bursts.begin(); iter != bursts.end(); ) {
        Burst *burst = *iter;
        
         if (burst->isReadyForDelete()) {
             Serial.println("Deleting burst");

            //Free up our memory
            delete burst;

            //Remove the burst from our vector and update our iterator.
            iter = bursts.erase(iter);
         } else {
             iter++;
         }
    }
}